#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import argparse
import itertools
import os
import os.path
import subprocess
import sys
import time
import yaml
import signal

poll = []

parser = argparse.ArgumentParser(
    description='Run stechec processes for a match',
    epilog='''Configuration file example:
    rules: libprologin2013.so
    map: ./simple.map
    verbose: 3
    clients:
      - ./champion.so
      - ./champion.so
    names:
      - Blue ones
      - Red ones
    spectators:
      - ./dumper.so
      - ./gui.so

Report bugs to <serveur@prologin.org>''',
    formatter_class=argparse.RawDescriptionHelpFormatter
)
parser.add_argument(
    '-v', '--version', action='store_true',
    help='Display version information'
)
parser.add_argument(
    '-n', '--inhibit', metavar='P', type=int, action='append', default=[],
    help='Do not start process number P (0 = server, 1=first client, ...'
    ' (champions first, spectators last)'
)
parser.add_argument(
    '-g', '--gdb', metavar='P', type=int, action='append', default=[],
    help='Debug process number P with gdb (0 = server, 1=first client, ...'
    ' (champions first, spectators last)'
)
parser.add_argument(
    'config-file', metavar='config-file',
    help='use F as a configuration file for the match'
)


def version():
    print('''stechec2-run.py

Copyright © 2013-2014, Prologin.
This is free software; see the source for copying conditions. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.''')


def stechec2_run(args, options):
    popt = {'stderr': sys.stdin, 'stdout': sys.stdout}
    server_opt = ['stechec2-server']
    client_opt = ['stechec2-client']

    # Used by both client and server
    for i in ['rules', 'verbose', 'map', 'time', 'memory']:
        if i in options:
            server_opt += ['--' + i, str(options[i])]
            client_opt += ['--' + i, str(options[i])]

    # Used by server only
    clients = options.get('clients', [])
    client_names = options.get('names', [])
    spectators = options.get('spectators', [])
    server_opt += ['--nb_clients', str(len(clients) + len(spectators))]

    next_process = iter(itertools.count())

    def start_proc(name, opts, popt):
        p = next(next_process)
        if p in args.gdb:
            opts = ['gdb', '--args'] + opts
        cmd_line = ' '.join(opts)
        if p in args.inhibit:
            print('>>> Not starting {}[P={}]:'.format(name, p))
            print('    {}'.format(cmd_line))
        else:
            print('>>> Starting {}[P={}]:'.format(name, p))
            print('    {}'.format(cmd_line))
            poll.append(subprocess.Popen(opts, **popt))

    # Start the server
    start_proc('server', server_opt, popt)
    # Let it start XXX: UGLY HACK. TO FIX
    time.sleep(1)

    def run_client(client, name, is_spectator):
        opts = client_opt + ['--champion', client, '--name', name]
        if is_spectator:
            opts.append('--spectator')
        client_popt = dict(popt)
        client_popt['env'] = dict(os.environ)
        client_popt['env']['CHAMPION_PATH'] = os.path.dirname(client)
        start_proc(name, opts, client_popt)

    # Start clients, then spectators
    for i, lib_so in enumerate(clients):
        lib_so = os.path.expanduser(lib_so)
        try:
            name = client_names[i]
        except IndexError:
            name = 'client-{}'.format(i + 1)
        run_client(lib_so, name, False)

    for i, lib_so in enumerate(spectators):
        lib_so = os.path.expanduser(lib_so)
        run_client(lib_so, 'spectator-{}'.format(i + 1), True)

    # Wait them all
    for p in poll:
        p.wait()


def kill_handler(signal, frame):
    for p in poll:
        p.kill()
    sys.exit(0)

signal.signal(signal.SIGINT, kill_handler)

if __name__ == '__main__':
    args = parser.parse_args()
    if args.version:
        version()
        sys.exit(0)
    else:
        config_file = vars(args)['config-file']
        try:
            c = yaml.load(open(config_file))
        except yaml.YAMLError as e:
            print('Parse error in {}: {}'.format(config_file, e))
            sys.exit(1)
        stechec2_run(args, c)
